# Podpora
┊|Aleš / Alenn|┊
:---|:---|---:
![Logo#1](https://cdn1.iconfinder.com/data/icons/hawcons/32/699902-icon-34-award-64.png) | **Pokud jsem Vám  ušetřil ČAS či ulehčil život a chcete se mi nějak odměnit, můžete následovně:**


┊Služba pro ![Donate#1](https://cdn2.iconfinder.com/data/icons/food-drink-10/24/food-drink-29-24.png)|Link ![Logo#2](https://cdn0.iconfinder.com/data/icons/glyphpack/52/link-rounded-20.png)|┊
:---|:---:|-
**PayPal:** | [![Donate#1](https://cdn4.iconfinder.com/data/icons/simple-peyment-methods/512/paypal_new-64.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LD9A9XDASUAAE)
**Ethereum:** |   ![Donate#2](http://i.imgur.com/lCRplpE.png) 0x90D2D091755F321927CFB416a171F522464059af
**Poděkování:** |   [![Mail#1](https://cdn3.iconfinder.com/data/icons/linecons-free-vector-icons-pack/32/mail-32.png)](mailto:Floryk4@gmail.com)



## ![Logo#3](https://cdn4.iconfinder.com/data/icons/flipicons-vol-2-2/512/Like-48.png) Předem děkuji za jakoukoli částku či reakci na mail.

---
Reason:

- Ušetření času = peněz, štěstí =)
- Materiály do školy
- Návod pro programování
- Video
- Návod do hry
- Jiné:

---

`Cizím lidem obvikle neposkytuji žádnou další podporu ale pokud máte zájem nebo mě jinak motivujete není problém vám věnovat trošku času. =)`